const Database = require('./Database');

exports.uiCalls = async (req, res)=>{
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.set('Access-Control-Allow-Headers', 'Content-Type');
  if(req.method === 'OPTIONS'){
    res.status(200).send();
    return
  }

  const operation = req.body.operation;

  if(!operation){
    res.status(400).send(`operation ${operation} is not valid`);
  }

  if(operation === 'getFamiliesObject'){
    const result = await Database.getFamiliesObject();

    res.status(200).send(result);
  }

  if(operation === 'getInteracPaymentsForPeriod'){
    const month = req.body.month;
    const year = req.body.year;
    const result = await Database.getInteracPaymentsForPeriod(month, year);

    res.status(200).send(result);
  }
};
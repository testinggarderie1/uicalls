const Datastore = require('@google-cloud/datastore');
const customDatastore = require('DataStore');
const datastore = new Datastore({
  projectId: 'manager-218900',
});

async function getInteracPaymentsForPeriod(month, year){
  let familyPaymentsObject = {payments:{}};
  const paymentSheetKey = datastore.key(['PaymentSheet', `PaymentSheet-${month}-${year}`]);
  const familyBillsQuery = datastore.createQuery('FamilyBill');
  familyBillsQuery.hasAncestor(paymentSheetKey);
  const [familyBills] = await datastore.runQuery(familyBillsQuery);
  for(const familyBill of familyBills){
    let familyBillObject = {familyId : familyBill.familyId};
    const familyBillKey = datastore.key(['PaymentSheet', `PaymentSheet-${month}-${year}`,
      'FamilyBill', familyBill[datastore.KEY].name]);

    const childBillQuery = datastore.createQuery('ChildBill');
    childBillQuery.hasAncestor(familyBillKey);
    const [childBills] = await datastore.runQuery(childBillQuery);

    for(const childBill of childBills){
      let childBillObject = {childID : childBill.childId, interacPayments:[]};
      const interacPaymentsQuery = datastore.createQuery('InteracPayment');
      const childBillKey = datastore.key(['PaymentSheet', `PaymentSheet-${month}-${year}`,
        'FamilyBill', familyBill[datastore.KEY].name, 'ChildBill', childBill[datastore.KEY].name]);
      interacPaymentsQuery.hasAncestor(childBillKey);
      const [interacPayments] = await datastore.runQuery(interacPaymentsQuery);
      interacPayments.forEach(
        interacPayment => childBillObject.interacPayments.push(
          {
            moneySent : interacPayment.moneySent,
            sender : interacPayment.sender,
            date : interacPayment.dateOfPayment,
            messageId : interacPayment.interacMessageId
          }));

      familyBillObject[childBill.childId] = childBillObject;
    }

    familyPaymentsObject[familyBill.familyId] = familyBillObject;

  }

  return familyPaymentsObject;

}

async function getFamiliesObject(){
  let familiesObject = {families:{}};
  const query = datastore.createQuery('Family');
  const [families] = await datastore.runQuery(query);
  for(const family of families){
    let familyObject = {familyId:"", childs:[], parents:[]};
    familyObject.familyId = family[datastore.KEY].name;

    const ancestorKey = datastore.key(['Family', familyObject.familyId]);
    const childsQuery = datastore.createQuery('Child');
    childsQuery.hasAncestor(ancestorKey);
    const [childs] = await datastore.runQuery(childsQuery);

    for (const child of childs) {
      let childObject = {};
      childObject.familyId = familyObject.familyId;
      childObject.birthDate = child.birthDate;
      childObject.lastName = child.lastName;
      childObject.endDate = child.endDate;
      childObject.firstName = child.firstName;
      childObject.startDate = child.startDate;
      childObject.sex = child.sex;
      familyObject.childs.push(childObject);
    }

    const parentsChilds = datastore.createQuery('Parent');
    parentsChilds.hasAncestor(ancestorKey);
    const [parents] = await datastore.runQuery(parentsChilds);

    for (const parent of parents) {
      let parentObject = {};
      parentObject.familyId = familyObject.familyId;
      parentObject.birthDate = parent.birthDate;
      parentObject.lastName = parent.lastName;
      parentObject.firstName = parent.firstName;
      parentObject.sex = parent.sex;
      parentObject.phone1 = parent.phone1;
      parentObject.mail1 = parent.mail1;
      familyObject.parents.push(parentObject);
    }
    familiesObject.families[familyObject.familyId] = familyObject;
  }
  console.log(familiesObject);
  return familiesObject;

}

async function testCustomDatastore(){
  const testRequest = {kind : 'FamilyBill', familyId: 'genie234', paymentSheetId: 'PaymentSheet-5-2019'};
  const res ={};
  await customDatastore.addEntity(testRequest, res);
}

testCustomDatastore();
module.exports = {getFamiliesObject , getInteracPaymentsForPeriod};